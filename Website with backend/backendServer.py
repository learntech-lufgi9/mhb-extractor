from flask import Flask, request, render_template, redirect, url_for, send_file, jsonify
import os
import json
from spacy.matcher import PhraseMatcher
import urllib.parse
import PyPDF2
import random
from collections import Counter
from werkzeug.utils import secure_filename
from werkzeug.serving import make_server
import spacy
import fitz
import re

app = Flask(__name__)
#app.config['UPLOAD_FOLDER'] = 'C:\\Users\\Max\\Desktop\\Website\\'
app.config['UPLOAD_FOLDER'] = os.getcwd()
new_nlp = spacy.load("Model/model-best")


#Base Funktion to extract Text using PyMuPDF and ignoring the header and footer
def get_text_from_pdf(filename, page_number=None):
    # All Text from PDF
    text_body = ""
    doc = fitz.open(filename)
    if page_number is not None:
        page = doc[page_number]
        mediabox = page.mediabox
        header_height = 50 # Höhe des Headers in Pixel
        footer_height = 50 # Höhe des Footers in Pixel
        body_rect = fitz.Rect(0, header_height, mediabox.width, mediabox.height - footer_height)
        text = page.get_text("text", clip=body_rect)
        text = text.replace("\n", " ")
        text_body += text
    else:
        for page in doc:
            mediabox = page.mediabox
            header_height = 50 # Höhe des Headers in Pixel
            footer_height = 50 # Höhe des Footers in Pixel
            body_rect = fitz.Rect(0, header_height, mediabox.width, mediabox.height - footer_height)
            text = page.get_text("text", clip=body_rect)
            text = text.replace("\n", " ")
            text_body += text

    return text_body






@app.route('/')
def upload():
    return render_template('Upload.html')

@app.route("/results", methods=["POST"])
def results():
    # PDF-Datei
    pdf_file = request.args.get('filename')
    #extract keywords defined in a json file from a text
    def extract_keywords(text, json_file):
        with open(json_file) as f:
            data = json.load(f)
            keywords = data["keywords"]

        nlp = spacy.load("de_core_news_lg")
        matcher = PhraseMatcher(nlp.vocab)

        # Add the keywords and synonyms to the phrase matcher
        patterns = [nlp(keyword) for keyword in keywords]
        matcher.add("Keywords", None, *patterns)

        # find all matches in the document
        keyword_counts = Counter()
        doc = nlp(text)
        matches = matcher(doc)
        for match_id, start, end in matches:
            keyword = doc[start:end].text
            keyword_counts[keyword] += 1

        return list(keyword_counts.items())

    # open pdf with pypdf2, content dont care
    pdf_files = open(pdf_file, "rb")
    pdf_reader = PyPDF2.PdfReader(pdf_files)

    # random page starting page 10
    page_num = random.randint(9, len(pdf_reader.pages) - 1)
    page_num_str = str(page_num + 2)

    # search for MODUL ents
    modul_ent = None
    for i in range(page_num, len(pdf_reader.pages)):
        # extracting text
        text = get_text_from_pdf(pdf_file, page_num)
        print(text)

        # open using spacy
        doc = new_nlp(text)

        # search for Keywords on ent page
        for ent in doc.ents:
            if ent.label_ == "MODUL":
                modul_ent = ent
                #this could be a variable later
                json_file = "Vorlage\Vorschlag\keywords.json"
                keywords_ent = extract_keywords(text, json_file)
                break
        if modul_ent is not None:
            break
    if modul_ent is None: #press the start button again
        print(f"Keine Modul-Entität gefunden auf Seiten ab {page_num + 1}")
        # genarate json for webview
        data = {
            "most_common_keyword": "try again " + str(i),
            "total_keyword_count": "0",
            "modul_entity": "NONE",
            "page_num": i + 2
        }
        #maybe needed later, room for improvement
        with open("vorschlag.json", 'w', encoding='utf-8') as f:
            json.dump(data, f,ensure_ascii=False)

        try:
            with open("vorschlag.json", encoding='utf-8') as f:
                data = json.load(f)
                return jsonify(data)
        except FileNotFoundError:
            return 'File not found', 404



    #text from complete file
    text_comp = get_text_from_pdf(pdf_file)

    json_file = "Vorlage\Vorschlag\keywords.json"
    keywords_ges = extract_keywords(text_comp, json_file)

    # match keywords from ent site and complete PDF
    common_keywords = []
    for keyword, count in keywords_ent:
        for kw, cnt in keywords_ges:
            if keyword == kw:
                common_keywords.append((keyword, count + cnt))

    print(common_keywords)


    # Keyword with the highest count and its frequency
    if common_keywords:
        counts = [count for keyword, count in common_keywords]
        most_common_count = max(counts)
        num_most_common = counts.count(most_common_count)
        if num_most_common == 1 or random.random() < 0.9:
            most_common_keyword = max(common_keywords, key=lambda x: x[1])[0]
            total_count = sum([count for keyword, count in common_keywords])
        else:
            # Choose a keyword with a count similar to the most common with 10% difference
            similar_counts = [count for count in counts if
                              most_common_count * 0.9 <= count <= most_common_count * 1.1]
            similar_keywords = [keyword for keyword, count in common_keywords if count in similar_counts]
            most_common_keyword = random.choice(similar_keywords)
            total_count = sum(similar_counts)
    else:
        most_common_keyword = None
        total_count = 0

    data = {
        "most_common_keyword": most_common_keyword,
        "total_keyword_count": common_keywords,
        "modul_entity": str(modul_ent.text),
        "page_num": i + 2
    }
#output for website
    with open("vorschlag.json", 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False)

    try:
        with open("vorschlag.json", encoding='utf-8') as f:
            data = json.load(f)
            return jsonify(data)
    except FileNotFoundError:
        return 'File not found', 404




#approute for render templates
@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/edit', methods=['GET'])
def edit():
    return render_template('edit.html')

@app.route('/guid', methods=['GET'])
def guid():
    return render_template('guid.html')

#approute for guided mode
@app.route('/guide', methods=['GET'])
def guide():
    #get regex and filename for separation
    regex = request.args.get('regex')
    filename = request.args.get('filename')
    print(filename)
    print(regex)

    pdf_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    doc = fitz.open(pdf_path)
    #
    for page in doc:
        # search
        search_results = re.findall(regex, page.get_text())

        # highlight
        for result in search_results:
            text_instances = page.search_for(result)
            for inst in text_instances:
                highlight = page.add_highlight_annot(inst)
                highlight.update()

    # save all files
    filename_parts = filename.split('.')
    filename2 = f"{filename_parts[0]}_.{filename_parts[1]}"
    doc.save(filename2)
#redirect to new page in guided mode
    return redirect(url_for('guid', filename=filename2))
#approute to cut pdf in one pdf per module
@app.route('/guid_cut', methods=['POST'])
def guid_cut():

    filename = request.args.get('filename')
#find all highlights
    def find_highlight_annotations(filename: object) -> object:
        highlights = []
        with fitz.open(filename) as doc:
            for page in doc:
                annot = page.first_annot
                while annot:
                    if annot.type[0] == 8:
                        highlights.append(str(annot))
                    annot = annot.next
        return highlights
#helper function
    def sort_numbers(numbers):
        return sorted(numbers)
#sort highlights by page
    highlights = find_highlight_annotations(filename)
    highlight_pages = set()
    for highlight in highlights:
        highlight_str = str(highlight)
        highlight_page_num = int(highlight_str.split(" ")[-3])
        if highlight_page_num not in highlight_pages:
            highlight_pages.add(highlight_page_num)
    sorted_highlights = sort_numbers(highlight_pages)
    print(sorted_highlights)
#create tupel for cutting
    def create_page_ranges(highlights):
        # leere Liste
        page_ranges = []
        # Gehe durch jedes Highlight
        for i in range(len(highlights)):
            # Wenn es das letzte Highlight ist, eine Seite dann Ende
            if i == len(highlights) - 1:
                start = highlights[i]
                end = highlights[i] + 1
                page_ranges.append((start, end))
                break
            #
            start = highlights[i]
            end = highlights[i + 1] - 1
            page_ranges.append((start, end))
        # return Liste
        return page_ranges

    list_pages = create_page_ranges(sorted_highlights)
    print(list_pages)
#create new pdfs from tupel and pdf
    def split_pdf_by_page_range(input_file, page_ranges):
        doc = fitz.open(input_file)
        for i, (start, end) in enumerate(page_ranges, start=1):
            selected_pages = []
            for page in range(start - 1, end):
                selected_pages.append(doc[page])
            output_file = f"{i}.pdf"
            doc_selected = fitz.open()
            doc_selected.insert_pdf(doc, from_page=start, to_page=end)
            doc_selected.save(output_file)
            print(f"Creating file: {output_file}")
            doc_selected.close()
        doc.close()
#execute
    split_pdf_by_page_range(filename, list_pages)

    return ''
#delete a specific highlight using coordinates in the pdf.js
@app.route('/del_spe_highlight', methods=['POST'])
def del_highlight():
    # Extract the filename and coordinates
    filename = request.args.get('filename')
    coords = request.args.get('coords')
    page_number = int(request.args.get('page', 1)) - 1
    # Parse the coordinates from the request
    coords = tuple(map(int, json.loads(coords)))

    # Open pdf
    doc = fitz.open(filename)

    # Get the pages coords
    x1, y1, x2, y2 = coords

    # Get the page and all annotations on it
    page = doc[page_number]
    annots = page.annots()
    for annot in annots:
        rect = annot.rect  #if coords with a difference of 5 match -> delete
        if abs(rect.x0 - x1) <= 5 and abs(rect.y0 - y1) <= 5 and abs(rect.x1 - x2) <= 5 and abs(rect.y1 - y2) <= 5:
            page.delete_annot(annot)

    # Save
    doc.save(filename, incremental=True, encryption=fitz.PDF_ENCRYPT_KEEP)

    # Close
    doc.close()

    return 'Highlight annotation deleted successfully.'

#add the highlights of this page to the keyword file, this is used as new_keys
@app.route('/highlight_to_keywords', methods=['POST'])
def add_highlight_to_keyword_file():
    # Extract the filename
    filename = request.args.get('filename')
    #Highlighted_text from json
    with open(filename+"_highlights.json", encoding='utf-8') as f:
        highlighted_text_dict = json.load(f)

    # load json for keywords
    with open('Vorlage\Keywords.json', encoding="utf-8") as f:
        keywords_dict = json.load(f)
#define spcy model, this is the large german model
    nlp = spacy.load('de_core_news_lg')
    # add words, just some examples. more needed
    Modul = nlp(u'Modul').vector
    Name = nlp(u'name').vector
    Namen = nlp(u'namen').vector
    Selbst = nlp(u'Selbst').vector
    Studium = nlp(u'Studium').vector

    Kredit = nlp(u'Kredit').vector
    Punkte = nlp(u'punkte').vector
    Arbeit = nlp(u'Arbeits').vector
    Aufwand = nlp(u'Aufwand').vector

    Modulname = Modul + Name
    Modulnamen = Modul + Namen
    Selbststudium = Selbst + Studium
    Modul_Nr = Modul + nlp(u'-').vector + nlp(u'Nr.').vector
    Kreditpunkte = Kredit + Punkte
    Arbeitsaufwand = Arbeit + Aufwand
    Moduldauer = Modul + nlp(u'-').vector + nlp(u'Dauer').vector

    vocab = nlp.vocab
#set new word vectors for compountwords
    vocab.set_vector(u'Modulname', Modulname)
    vocab.set_vector(u'Selbststudium', Selbststudium)
    vocab.set_vector(u'Modul-Nr', Modul_Nr)
    vocab.set_vector(u'Kreditpunkte', Kreditpunkte)
    vocab.set_vector(u'Arbeitsaufwand', Arbeitsaufwand)
    vocab.set_vector(u'Moduldauer', Moduldauer)

    # add to syn
    def add_synonym(keyword, synonym):
        if "Synonym1" in keywords_dict['synonyms'][keyword]:
            keywords_dict['synonyms'][keyword].remove("Synonym1")
            print(synonym)
        if synonym not in keywords_dict['synonyms'][keyword]:
            keywords_dict['synonyms'][keyword].append(synonym)
            print("newsyn: " + synonym)
#add as keyword
    def add_new_keywords(token):
        if token.text not in keywords_dict['keywords'] and token.text not in keywords_dict['synonyms']:
            keywords_dict['keywords'].append(token.text)
            print(token.text)
#check for similarity
    for key, value in highlighted_text_dict.copy().items(): #use copy as list may gets bigger
        text = value['highlighted_text']
        token = nlp(text.strip())
        for keyword, synonyms in keywords_dict['synonyms'].copy().items():
            if keyword in highlighted_text_dict:
                continue
            else:
                similarity = token.similarity(nlp(keyword))
                if similarity < 1.0 and similarity >= 0.70:
                    add_synonym(keyword, str(token))
                    break
        else:
            if keyword not in keywords_dict and str(token) not in [syn for syn_list in keywords_dict['synonyms'].values() for syn in syn_list]:
                add_new_keywords(token)
#save new json
        with open("keywords.json", 'w', encoding='utf-8') as f:
            json.dump(keywords_dict, f, ensure_ascii=False)

    return 'New Keyword file saved'
#add a highlight using coords drom pdf.js and pdfannotate.js
@app.route('/add_highlight', methods=['POST'])
def add_highlight():
    # Extract the filename and coordinates
    filename = request.args.get('filename')
    coords = request.args.get('coords')
    page_number = int(request.args.get('page', 1)) - 1
    # Parse the coordinates from the request
    coords = tuple(map(int, json.loads(coords)))

    # Open pdf
    doc = fitz.open(filename)

    # Get the pages coords
    x1, y1, x2, y2 = coords

    # Create the highligh
    page = doc[page_number]


    highlight = page.add_highlight_annot((x1, y1, x2, y2))
    highlight.update()

    # Save
    doc.save(filename, incremental=True, encryption=fitz.PDF_ENCRYPT_KEEP)

    # Close
    doc.close()

    return 'Highlight annotation added successfully.'
#extract text between highlights and output as json for website
@app.route('/get_list', methods=['POST'])
def get_list():
    # Extract the filename
    filename = request.args.get('filename')
    #page_number = int(request.args.get('page', 1)) - 1

    #Highlighted_text from json
    with open(filename+"_highlights.json", encoding='utf-8') as f:
        data = json.load(f)

    highlighted_text_list = []

    for obj in data.values():
        highlighted_text_list.append(obj['highlighted_text'])

    # All Text from PDF
    text_body = get_text_from_pdf(filename)

    # loop through the keys in highlighted_text_list and find the corresponding values, later spacy is used
    result = []
    for i in range(len(highlighted_text_list)):
        key = highlighted_text_list[i]
        if i < len(highlighted_text_list) - 1:
            next_key = highlighted_text_list[i + 1]
            pattern = re.escape(key) + r'(.*?)' + re.escape(next_key)
        else:
            pattern = re.escape(key) + r'(.*)'
        match = re.search(pattern, text_body, re.DOTALL)
        if match:
            value = match.group(1).strip()
            result.append((key, value))

    #fix for special key
    for i in range(len(result)):
        if result[i][0] == " ":
            result[i] = ("NAME MODUL",) + result[i][1:]

    #save to json
    with open(f"{filename}_highlighted_keywords.json", 'w', encoding='utf-8') as f:
        json.dump(result, f, ensure_ascii=False)


    return 'successfully.'
#add highlights to pdf using keyword list, trained model not working properly
@app.route('/add_highlights', methods=['POST'])
def add_highlightsfromkeywords():
    nlp = spacy.load('de_core_news_lg')
    filename = request.args.get('filename')
    boldflag = request.args.get('boldflag')
    newkeys = request.args.get('newkeys')
    print(boldflag)
#test if new keyfile should be used
    if newkeys == "true":
        with open('keywords.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
    else:
        with open('Vorlage\Keywords.json', 'r', encoding='utf-8') as f:
            data = json.load(f)


    keywords = data['keywords']
    synonyms = data['synonyms']

    # open pdf and del all highlights
    doc = fitz.open(filename)
    for page in doc:
        while page.first_annot:
            page.delete_annot(page.first_annot)
#use phrasmatcher, not trained model. Makes finding single words much easier
    matcher = PhraseMatcher(nlp.vocab)
    found_words = set()
    # Add the keywords and synonyms to the phrase matcher
    patterns = [nlp(keyword) for keyword in keywords]
    for keyword, synonyms_list in synonyms.items():
        patterns.append(nlp(keyword.strip()))
        patterns.extend([nlp(synonym.strip()) for synonym in synonyms_list])
    matcher.add("Keywords", None, *patterns)
#loop using the blocks of the page and flags for different font types, e.g bold etc.
    for page in doc:
        blocks = page.get_text("dict", flags=11)["blocks"]
        for b in blocks:  # iterate through the text blocks
            for l in b["lines"]:  # iterate through the text lines
                for s in l["spans"]:  # iterate through the text spans
                    if boldflag == "true":
                        if s["flags"] == 20 or s["flags"] == 16:
                            text = s["text"]
                            docs = nlp(text)
                            # Use the phrase matcher to find matching keywords and synonyms
                            matches = matcher(docs)
                            for match_id, start, end in matches:
                                matched_word = docs[start:end].text
                                print(matched_word)
                                if matched_word not in found_words:  # Only mark the first occurrence of the word
                                    # Mark the matching text
                                    highlight = page.add_highlight_annot(fitz.Rect(s["bbox"]))
                                    highlight.update()
                                    found_words.add(matched_word)
                    else: #dont use font type information
                        text = s["text"]
                        docs = nlp(text)
                        # Use the phrase matcher to find matching keywords and synonyms
                        matches = matcher(docs)
                        for match_id, start, end in matches:
                            matched_word = docs[start:end].text
                            print(matched_word)
                            if matched_word not in found_words:  # Only mark the first occurrence of the word
                                # Mark the matching text
                                highlight = page.add_highlight_annot(fitz.Rect(s["bbox"]))
                                highlight.update()
                                found_words.add(matched_word)

    # Save
    doc.save(filename, incremental=True, encryption=fitz.PDF_ENCRYPT_KEEP)

    # Close
    doc.close()

    return 'Highlight annotation added successfully for complete File.'

#returns last highlight to webbrowser, not in use
@app.route('/get_last_highlights', methods=['POST'])
def get_highlights():
    filename = request.args.get('filename')

    doc = fitz.open(filename)

    ret = []

    for pg in range(doc.page_count):
        page = doc[pg]
        last_annot = None
        for annot in page.annots():
            last_annot = annot
            # correction
            x1, y1, x2, y2 = last_annot.rect
            x1 = x1 - 4
            y1 = y1 - 4
            x2 = x2 + 4
            y2 = y2 + 4
            tup = (x1, y1, x2, y2)
            ret = str(page.get_textbox(tup))

    doc.close

    return ret
#function to generate last output as json, can be called from any module pdf
@app.route('/all', methods=['POST'])
def get_all():
    filename = request.args.get('filename')
    results = []
    special_flag = False # sonderfall, erster Key ist über * angegeben auf website. Alles bis zum ersten key ist der Name des Moduls
    #open highlights from file with special keys
    with open(filename+"_highlights.json", encoding='utf-8') as f:
        data = json.load(f)
    #all pdfs containing a number as name
    for filenames in os.listdir('.'):
        if filenames.endswith(".pdf") and any(char.isdigit() for char in filenames) and len(filenames) <= 7:
            highlighted_text_list = []

            for obj in data.values():
                if obj['highlighted_text'] == " ":
                    special_flag = True
                highlighted_text_list.append(obj['highlighted_text'].strip())

            highlighted_text_list = list(filter(lambda x: x.strip(), highlighted_text_list))
            #check contents of highlighted_text_list


            #all Text from PDF without header and footer
            text_body = get_text_from_pdf(filenames)

            nlp = spacy.load('de_core_news_lg')

            patterns = []
            for text in highlighted_text_list:
                pattern = []
                text = text.replace(" : ", ":") # sort out wrong formatting
                for word in text.split():
                    if len(word) > 1:  # just words with more than 1 letter, errors will be corrected this way
                        if '/' in word:
                            match = re.match(r'(\w+)/(\w+)', word)
                            if match:
                                word1, word2 = match.groups()
                                pattern += [{"LOWER": word1.lower()}, {"LOWER": "/"}, {"LOWER": word2.lower()}]
                        elif '-' in word:
                            match = re.match(r'(\w+)-(\w+).', word)
                            if match:
                                pattern += [{"LOWER": word.lower().replace('.', '')},  {"IS_PUNCT": True}]
                        elif ':' in word:
                            match = re.match(r'(\w+):(\w+)', word)
                            if match:
                                word1, word2 = match.groups()
                                pattern += [{"LOWER": word1.lower()}, {'SPACY': True}]
                            else:
                                pattern.append({"LOWER": word.lower().replace(':', '')})
                        else:
                            pattern.append({"LOWER": word.lower()})
                if pattern:
                    patterns.append(pattern)

            matcher = spacy.matcher.Matcher(nlp.vocab)
            for i, pattern in enumerate(patterns):
                matcher.add(f"pattern_{i}", [pattern])

            doc = nlp(text_body)
            matches = matcher(doc)
            print(matches)

            result = []

            #key for special case:
            if special_flag == True:
                match = matches[0]
                start_index = match[1]
                text_before_match = doc[:start_index].text
                key = "Name Modul"
                value = text_before_match
                result.append((key, value))
#create tupel(key,value) with value as text between two keys
            found_keys = set()
            for i, (match_id, start, end) in enumerate(matcher(doc)):
                key = doc[start:end].text
                if key not in found_keys:
                    found_keys.add(key)
                    if i < len(matches) - 1:
                        next_start = matcher(doc)[i + 1][1]
                        value = doc[end:next_start].text.strip().replace(':', '')
                    else:
                        value = doc[end:].text.strip()
                    result.append((key, value))
                else:
                    if i < len(matches) - 1:
                        next_start = matcher(doc)[i + 1][1]
                        value = doc[end:next_start].text.strip().replace(':', '')
                    else:
                        value = doc[end:].text.strip().replace(':', '')
                    result[-1] = (result[-1][0], result[-1][1] + ' ' + key + ' ' + value)
            print(result)
            results.append(result)
    # filename from folder
    filename = ""
    for filenames in os.listdir('.'):
        if filenames.endswith(".pdf") and len(filenames) >= 7:
            base_filename = os.path.splitext(filenames)[0]
            filename = base_filename + '_all_highlights.json'
    # save to json
    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(results, f, ensure_ascii=False)
#send to download in webbrowser
    return jsonify(filename=filename)

#delete last set highlight in pdf
@app.route('/del_highlights', methods=['POST'])
def del_highlights():
    filename = request.args.get('filename')

    doc = fitz.open(filename)
#find last annot
    for pg in range(doc.page_count):
        page = doc[pg]
        last_annot = None
        for annot in page.annots():
            last_annot = annot
#del last annot
        if last_annot:
            page.delete_annot(last_annot)
#save incr. file
    doc.save(filename, incremental=True, encryption=fitz.PDF_ENCRYPT_KEEP)
#close doc
    doc.close
#return success
    return jsonify(success=True)
#name is wrong, because sorting is no longer needed. Just adds the new keyfile to the space that is not erased
@app.route('/sort_highlights', methods=['POST'])
def sort_json_highlights():
    filename = request.args.get('filename')

    with open(f"keywords.json", 'r', encoding='utf-8') as f:
        data = json.load(f)

    with open("Vorlage\Keywords.json", 'w', encoding='utf-8') as f:
        json.dump(data, f ,ensure_ascii=False)

    return jsonify(success=True)
#genarate a json file for all highlights on a given pdf. later used for the all function
@app.route('/save_highlights', methods=['POST'])
def save_highlights():

    highlighted_words = {}
    special_key = request.args.get('specialKey')
    filename = request.args.get('filename')
    keys = [key for key in special_key.split(',')]
#special case, maybe replaced with model if better trained
    for key in keys:
            decoded_key = urllib.parse.unquote(key)
            if key == "*":
                highlighted_words["Name"] = {"coords": "", "pages": "1", "highlighted_text": " "}
            elif key != "":
                highlighted_words[decoded_key] = {"coords": "", "pages": "1", "highlighted_text": decoded_key}

#open pdf and find all highlights, save to highlights
    with fitz.open(filename) as doc:
        for pg in range(doc.page_count):
            page = doc[pg]
            highlights = []
            annot = page.first_annot
            while annot:
                if annot.type[0] == 8:
                    all_coordinates = annot.vertices
                    if len(all_coordinates) == 4:
                        highlight_coord = fitz.Quad(all_coordinates).rect
                        highlights.append((page.number, highlight_coord))  # with pagenumber
                    else:
                        all_coordinates = [all_coordinates[x:x + 2] for x in range(0, len(all_coordinates), 2)]
                        for i in range(0, len(all_coordinates)):
                            coord = fitz.Quad(all_coordinates[i]).rect
                            highlights.append((page.number, coord))  # with pagenumber
                annot = annot.next
#highlights to highlighted_word dict for json
            highlight_text = []
            for page_num, h in highlights:
                sentence = []
                page = doc[page_num]
                #correction
                x1 = h.x0 - 4
                y1 = h.y0 - 4
                x2 = h.x1 + 4
                y2 = h.y1 + 4
                tup = (x1,y1,x2,y2)

                textbox = page.get_textbox(tup)

                sentence.append(textbox)
                if sentence:
                    key = " ".join(sentence)
                    coord_list = [x1, y1, x2, y2]
                    if key in highlighted_words:
                        continue
                    else:
                        highlighted_words[key] = {"coords": coord_list, "pages": pg+1, "highlighted_text": " ".join(sentence)}
#save json
    with open(f"{filename}_highlights.json", 'w', encoding='utf-8') as f:
        json.dump(highlighted_words, f, ensure_ascii=False)
#return
    return jsonify(success=True)

#method for the upload and cleanup
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':

        # cleanup
        folder = os.getcwd()
        for filename in os.listdir(folder):
            if filename.endswith(".pdf") or filename.endswith(".json"):
                os.remove(os.path.join(folder, filename))

        #save pdf file
        file = request.files['pdf-file']
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

#return filename for edit page
        return redirect(url_for('edit', filename=filename))
    return render_template('edit.html')
#all filenames on root directory
@app.route('/<filename>', methods=['GET'])
def get_uploaded_file(filename):
    try:
        return send_file(filename, as_attachment=filename)
    except FileNotFoundError:
        return 'File not found', 404
#return json with all values for preview in webbrowser
@app.route('/table', methods=['GET'])
def display_table():
    filename = request.args.get('filename')
    json_filename = filename + '_highlighted_keywords.json'
    try:
        with open(json_filename, encoding='utf-8') as f:
            data = json.load(f)
            return jsonify(data)
    except FileNotFoundError:
        return 'File not found', 404
#old method fur cut function on expert mode page
@app.route('/storage', methods=['POST'])
def split_pdf_and_store():
    regex = request.args.get('regex')
    filename = request.args.get('filename')

    pdf_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    doc = fitz.open(pdf_path)

    for page in doc:
        # Durchsuchen
        search_results = re.findall(regex, page.get_text())

        # Hervorheben
        for result in search_results:
            text_instances = page.search_for(result)
            for inst in text_instances:
                highlight = page.add_highlight_annot(inst)
                highlight.update()

    # Speichern
    filename_parts = filename.split('.')
    filename2 = f"{filename_parts[0]}_.{filename_parts[1]}"
    doc.save(filename2)
    highlights: list[Any] = []

    def find_highlight_annotations(filename: object) -> object:
        highlights = []
        with fitz.open(filename) as doc:
            for page in doc:
                annot = page.first_annot
                while annot:
                    if annot.type[0] == 8:
                        highlights.append(str(annot))
                    annot = annot.next
        return highlights

    def sort_numbers(numbers):
        return sorted(numbers)

    highlights = find_highlight_annotations(filename2)
    highlight_pages = set()
    for highlight in highlights:
        highlight_str = str(highlight)
        highlight_page_num = int(highlight_str.split(" ")[-3])
        if highlight_page_num not in highlight_pages:
            highlight_pages.add(highlight_page_num)
    sorted_highlights = sort_numbers(highlight_pages)
    print(sorted_highlights)

    def create_page_ranges(highlights):
        # leere Liste
        page_ranges = []
        # Gehe durch jedes Highlight
        for i in range(len(highlights)):
            # Wenn es das letzte Highlight ist einer Seite, dann Ende
            if i == len(highlights) - 1:
                start = highlights[i]
                end = highlights[i] + 1
                page_ranges.append((start, end))
                break
            #
            start = highlights[i]
            end = highlights[i + 1] - 1
            page_ranges.append((start, end))
        # return Liste
        return page_ranges

    list_pages = create_page_ranges(sorted_highlights)
    print(list_pages)

    def split_pdf_by_page_range(input_file, page_ranges):
        doc = fitz.open(input_file)
        for i, (start, end) in enumerate(page_ranges, start=1):
            selected_pages = []
            for page in range(start - 1, end):
                selected_pages.append(doc[page])
            output_file = f"{i}.pdf"
            doc_selected = fitz.open()
            doc_selected.insert_pdf(doc, from_page=start, to_page=end)
            doc_selected.save(output_file)
            #print(f"Creating file: {output_file}")
            doc_selected.close()
        doc.close()

    split_pdf_by_page_range(filename2, list_pages)

    return redirect(url_for('index', filename=filename2))

#return all files in _bundels directory for pdf tools
@app.route('/_bundles/<filename>', methods=['GET'])
def get_uploaded_files(filename):
    try:
        return send_file(f'./_bundles/{filename}', as_attachment=filename)
    except FileNotFoundError:
        return 'File not found', 404

#main
if __name__ == '__main__':
    # Server starten
    server = make_server('localhost', 5000, app)
    print("Webserver gestartet.")
    server.serve_forever()
